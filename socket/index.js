import * as config from "./config";
import {MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME} from "./config";

export const rooms = [];
export const users = new Set();
let activeRoom = null;


export default io => {
  io.on("connection", socket => {

    const username = socket.handshake.query.username;

    if (users.has(username)) {
      socket.emit("ERROR", {
        error: "user",
        message: "User already exists"
      });
    }

    socket.emit("UPDATE_ROOMS", rooms);
    users.add(username);

    socket.on("ADD_ROOM", (name) => {

      if (findRoom(name, rooms)) {
        socket.emit("ERROR", {
          error: "room",
          message: "Room already exists"
        });
      } else {
        rooms.push({
          name,
          usersCount: 0,
          users: [],
          id: socket.id,
          results: []
        });

        socket.emit("UPDATE_ROOMS", rooms);
        socket.broadcast.emit("UPDATE_ROOMS", rooms);

        const room = findRoom(name, rooms);
        socket.emit("ADD_ROOM_DONE", room);
      }
    })

    socket.on("JOIN_ROOM", (room) => {
      if (room.usersCount == MAXIMUM_USERS_FOR_ONE_ROOM) {
        socket.emit("ERROR", {
          error: "maximumUsers",
          message: "This room is already full. Choose another one"
        });
        return;
      }

      socket.join(room.name);
      activeRoom = findRoom(room.name, rooms);

      activeRoom.usersCount++;
      activeRoom.users.push({
        name: username,
        readyStatus: false,
        progressStatus: 0
      });
      socket.emit("RENDER_ROOM", activeRoom);
      socket.to(room.name).emit("RENDER_ROOM", activeRoom);

      socket.on("UPDATE_ROOM", (readyStatus) => {
        findUser(username, activeRoom.users).readyStatus = readyStatus;
        socket.emit("RENDER_ROOM", activeRoom);
        socket.to(room.name).emit("RENDER_ROOM", activeRoom);
      })

      socket.on("USERS_READY", () => {
        socket.emit("START_TIMER", SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME, 0);
        socket.to(room.name).emit("START_TIMER", SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME, 0);
      });

      socket.on("UPDATE_PROGRESS", (progressPercent) => {
        const user = findUser(username, activeRoom.users);

        user.progressStatus = progressPercent;
        if (user.progressStatus == 100) {
          activeRoom.results.push(user);
        }
        socket.emit("UPDATE_USERS_INFO", activeRoom);
        socket.to(room.name).emit("UPDATE_USERS_INFO", activeRoom);
      });

      socket.on("END_GAME", () => {
        const filteredResults = activeRoom.users.sort(({progressStatus: a}, {progressStatus: b}) => b - a);
        for (let filteredResult of filteredResults) {
          if (!findUser(filteredResult.name, activeRoom.results)) {
            activeRoom.results.push(filteredResult);
          }
        }
        socket.emit("SHOW_RESULTS", activeRoom.results);
        socket.to(room.name).emit("SHOW_RESULTS", activeRoom.results);
      })

      socket.on("DISCARD_CHANGES", () => {
        activeRoom.results = [];
        for (let user of activeRoom.users) {
          user.progressStatus = 0;
          user.readyStatus = false;
        }

        socket.emit("RENDER_ROOM", activeRoom);
      })

    });

    socket.on("disconnect", () => {
      console.log(username, socket.id, 'disconnected');
      users.delete(username);
    })

  });
};

function findRoom(room, rooms) {
  return rooms.find(({name}) => name === room);
}

function findUser(username, users) {
  return users.find(({name}) => name === username);
}






