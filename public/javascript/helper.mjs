export const createElement = ({ tagName, className, attributes = {} }) => {
    const element = document.createElement(tagName);

    if (className) {
        addClass(element, className);
    }

    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
};

export const addClass = (element, className) => {
    const classNames = formatClassNames(className);
    element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
    const classNames = formatClassNames(className);
    element.classList.remove(...classNames);
};

export const getText = async (id) => {
    const response = await fetch(`/game/texts/`);
    const text = await response.text();
    return text;
}

export const formatClassNames = className => className.split(" ").filter(Boolean);


export function areReady(users) {
    let areReady = true;
    for (let {readyStatus} of users) {
        if (!readyStatus) {
            areReady = false;
        }
    }
    return areReady;
}


export function showModal({ title, bodyElement, onClose = () => {} }) {
    const root = getModalContainer();
    const modal = createModal({ title, bodyElement, onClose });

    root.append(modal);
}

function getModalContainer() {
    return document.getElementById('game-page');
}

function createModal({ title, bodyElement, onClose }) {
    const layer = createElement({ tagName: 'div', className: 'modal-layer' });
    const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
    const header = createHeader(title, onClose);

    modalContainer.append(header, bodyElement);
    layer.append(modalContainer);

    return layer;
}

function createHeader(title, onClose) {
    const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
    const titleElement = createElement({ tagName: 'span' });
    const closeButton = createElement({ tagName: 'button', className: 'close-btn', attributes: {id: 'quit-results-btn'} });

    titleElement.innerText = title;
    closeButton.innerText = '×';

    const close = (event) => {
        event.preventDefault();
        hideModal();
        onClose();
    }
    closeButton.addEventListener('click', close);
    headerElement.append(titleElement, closeButton);

    return headerElement;
}

function hideModal() {
    console.log('hide modal');
    const modal = document.getElementsByClassName('modal-layer')[0];
    modal?.remove();
}