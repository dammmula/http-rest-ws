import {
  addClass, createElement, getText,
  removeClass,
  // findRoom, findUser,
  areReady, showModal
} from "./helper.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

let roomsList = [];
let activeRoom = null;
let intervalID;
let keyupListener;

socket.on("ERROR", handleError);
socket.on("ADD_ROOM_DONE", (room) => socket.emit("JOIN_ROOM", room));
socket.on("UPDATE_ROOMS", updateRooms);
socket.on("RENDER_ROOM", renderRoom);
socket.on("START_TIMER", startTimer);
socket.on("UPDATE_USERS_INFO", updateProgressBar);
socket.on("SHOW_RESULTS", showResults);

const createRoomBtn = document.getElementById('add-room-btn');
createRoomBtn.addEventListener("click", addRoom);


function addRoom() {

  const name = prompt('Enter room name');

  if (!name.trim().length) {
    alert(("Invalid room name!"));
  } else if (findRoom(name, roomsList)) {
    alert("Room already exists");
  } else {
    socket.emit("ADD_ROOM", name);
  }
}


function updateRooms(rooms) {
  roomsList = rooms

  const roomsContainer = document.getElementById("rooms");
  roomsContainer.innerHTML = "";

  for (let room of rooms) {
    const roomElement = `<div class="col-sm-3">
            <div class="card room">
              <div class="card-body">
                <h5 class="card-title">Room #${room.name}</h5>
                <p class="card-text">${room.usersCount} users connected</p>
                <button class="btn btn-primary join-btn">Join</button>
              </div>
            </div>
          </div>`;

    roomsContainer.insertAdjacentHTML('beforeend', roomElement);
    const joinBtns = roomsContainer.querySelectorAll('.join-btn');
    for (let joinBtn of joinBtns) {
      joinBtn.addEventListener('click', () => {
        socket.emit("JOIN_ROOM", room);
      });
    }

  }

}

function handleError({error, message}) {
  alert(message);
  if (error === 'user') {
    sessionStorage.removeItem('username');
    window.location.replace("/login");
  }
}

function renderRoom(room) {
  activeRoom = room;

    const roomsPage = document.getElementById('rooms-page');
    const gamePage = document.getElementById('game-page');
    const user = findUser(username, activeRoom.users);
    const readyBtnText = user.readyStatus ? 'Not ready' : 'Ready';

    const roomsElement = `<div class="container container-fluid ">
      <div id="left-panel">
        <h1>Room #${room.name}</h1>
        <button id="quit-room-btn" class="btn btn-primary">< Back to rooms</button>
        <ul id="users-list">
        </ul>
      </div>

      <div id="game-container" class="card">
        <div id="text-container" class="display-none"></div>
        <div id="timer" class="display-none"></div>
        <button id="ready-btn" class="btn btn-primary">${readyBtnText}</button>
      </div>
    </div>`;

    gamePage.innerHTML = "";
    gamePage.insertAdjacentHTML('beforeend', roomsElement);

    const readyBtn = document.getElementById('ready-btn');
    const backToRoomsBtn = document.getElementById('quit-room-btn');

    const usersList = document.getElementById("users-list");
    for (let {name, readyStatus} of room.users) {
      const readyBtnClass = readyStatus ? 'ready-status-green' : 'ready-status-red';
      const youLabel = (name === username) ? '(you)' : '';
      const li = `<li>
            <div><i class="fas fa-circle ${readyBtnClass}"></i>${name} ${youLabel}</div>
            <div class="user-progress ${name}">
              <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow=1" aria-valuemin="0" aria-valuemax="100" style="width: 0"></div>
              </div>
            </div>
          </li>`


      usersList.insertAdjacentHTML('beforeend', li);
    }

    addClass(roomsPage, 'display-none');
    removeClass(gamePage, 'display-none');

    backToRoomsBtn.addEventListener('click', () => {
      addClass(gamePage, 'display-none');
      removeClass(roomsPage, 'display-none');
      // activeRoom = null;
      socket.emit("LEAVE_ROOM");
    });

    readyBtn.addEventListener('click', () => {
      user.readyStatus = !user.readyStatus;

      socket.emit("UPDATE_ROOM", user.readyStatus);
      if (areReady(activeRoom.users)) {
        socket.emit("USERS_READY");
      }
    });
}

async function startTimer(timeBeforeGame, gameTime, randomID) {
  const timer = document.getElementById('timer');
  const quitBtn = document.getElementById('quit-room-btn');
  const readyBtn = document.getElementById('ready-btn');

  readyBtn.style.display = "none";
  quitBtn.style.display = "none";

  removeClass(timer, 'display-none');
  timer.innerHTML = `${timeBeforeGame}`;

  const intervalID = setInterval(() => {
    timer.innerHTML = `${--timeBeforeGame}`;
    if (timeBeforeGame === 0) {
      startGame(gameTime, text);
      clearInterval(intervalID);
    }
  }, 1000);

  const text = await getText(randomID);
}

function startGame(timeLeft, text) {
  const timer = document.getElementById('timer');
  const gameContainer = document.getElementById('game-container');
  const textContainer = document.getElementById('text-container');
  const time = createElement({tagName: 'div', className: 'time-left-label'});
  const selectedText = createElement({tagName: 'span', className: 'selected-text'});
  const leftText = createElement({tagName: 'span', className: 'left-text'});

  const length = text.length;

  removeClass(textContainer, 'display-none');
  addClass(timer, 'display-none');

  selectedText.innerHTML = '';
  leftText.innerHTML = text;
  textContainer.append(selectedText, leftText);
  gameContainer.prepend(time);
  time.innerHTML = `${timeLeft} seconds left`;


  intervalID = setInterval(() => {
    time.innerHTML = `${--timeLeft} seconds left`;

    if (timeLeft === 0) {
      socket.emit("END_GAME");
      clearInterval(intervalID);
    }
  }, 1000);

  keyupListener = (event) => {
    if (event.key === leftText.innerHTML[0]) {

      selectedText.innerHTML = selectedText.innerHTML + event.key;
      leftText.innerHTML = leftText.innerHTML.slice(1, leftText.innerHTML.length + 1);

      const progressPercent = Math.round(selectedText.innerHTML.length * 100 / length);
      socket.emit("UPDATE_PROGRESS", progressPercent);
    }
  }

  document.addEventListener('keyup', keyupListener);
}



function updateProgressBar({ users }) {
  console.log('update progress bar');
  let allFinished = true;
  for (let user of users) {
    const progressBar = document.querySelector(`.user-progress.${user.name} .progress-bar`);
    progressBar.ariaValuenow = `${user.progressStatus}`;
    progressBar.style.width = `${user.progressStatus}%`;
    if (user.progressStatus == 100) {
      removeClass(progressBar, 'bg-warning');
      addClass(progressBar, 'bg-success');
    } else {
      allFinished = false;
    }
  }

  if (allFinished) {
    socket.emit("END_GAME");
  }
}

function showResults(results) {
  document.removeEventListener('keyup', keyupListener);
  if (intervalID) {
    clearInterval(intervalID);
  }

  const ul = createElement({tagName: 'ul', className: 'list-group'});
  let index = 1;
  for (let user of results) {
    const li = createElement({tagName: 'li', className: 'list-group-item', attributes: {id: `place-${index}`}});
    li.innerHTML = `${index++}. ${user.name}`;
    ul.append(li);
  }

  const args = {
    title: 'Results',
    bodyElement: ul,
    onClose: onModalClose
  }
  showModal(args);
}

function onModalClose() {
  socket.emit("DISCARD_CHANGES")
}

export function findRoom(room, rooms) {
  return rooms.find(({name}) => name === room);
}

export function findUser(username, users) {
  return users.find(({name}) => name === username);
}